import React, { useEffect, useState } from 'react'
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBan, faTrash, faWarning } from '@fortawesome/free-solid-svg-icons'
import { faChartSimple, faCrow, faGear, faTreeCity, faUser } from "@fortawesome/free-solid-svg-icons";
import {
    FaBox,
    FaCross,
    FaEnvelope,
    FaFacebook,
    FaInfo,
    FaKey,
    FaMoneyBill,
    FaSearch,
    FaTrash,
    FaUser,
    FaXbox,
} from "react-icons/fa";
import { setSelectionRange } from '@testing-library/user-event/dist/utils';
import { CREATE_PRODUCT, UPDATE_PRODUCT_BY_ID } from '../services/productService';
import { ThreeDots } from 'react-loader-spinner';
import { toast } from 'react-toastify';
import { Form } from 'react-bootstrap';
import { UPLOAD_FILE } from '../services/fileService';


const ProductModal = ({ updatedProduct, showProduct, handleCloseProductForm }) => {



    const [title, setTitle] = useState(updatedProduct ? updatedProduct.title : "")
    const [price, setPrice] = useState(updatedProduct ? updatedProduct.price : 0)
    const [description, setDescription] = useState(updatedProduct ? updatedProduct.description : "")
    const [categoryId, setCategoryId] = useState(updatedProduct ?
        updatedProduct.category.id : 4) // create by our own (shoes)
    const [images, setImages] = useState()
    const [isLoading, setIsLoading] = useState(false)



    // for fileupload 
    const [selectedFile, setSelectedFile] = useState(null)
    const [selectedImage, setSelectedImage] = useState(null)


    useEffect(() => {

        setTitle(updatedProduct ? updatedProduct.title : "")
        setDescription(updatedProduct ? updatedProduct.description : "")
        setPrice(updatedProduct ? updatedProduct.price : 0)
        setCategoryId(updatedProduct ? updatedProduct.category.id : 4)
        setImages(updatedProduct ? updatedProduct.images : [])
        setSelectedImage(updatedProduct ? updatedProduct.images[0] : "https://cdn3d.iconscout.com/3d/premium/thumb/image-6073772-4996992.png")


    }, [updatedProduct])


    // {
    //     "title": "Change title",
    //     "description": "updated!! ",
    //     "categoryId":4,
    //     "price": 100,
    //     "images": [
    //         "https://placeimg.com/640/480/any"
    //     ]
    // }




    const handleProductClose = () => {
        handleCloseProductForm(false)
    }
    let product = {
        title,
        price,
        description,
        categoryId,
        images: [
            "https://i0.wp.com/thinkfirstcommunication.com/wp-content/uploads/2022/05/placeholder-1-1.png?fit=1200%2C800&ssl=1"
        ]
    }

    const handleCreateNewProduct = () => {
        setIsLoading(true)

        if (updatedProduct) {

            if (selectedFile) {
                // reupload the new 
                
                let file = new FormData()
                file.append("file", selectedFile)

                    UPLOAD_FILE(file).then(
                        response => {

                            console.log("REsponsen UPdated Upload file : ", response.location)
                            // setImages([])
                            // setImages(images.push(response.location))
                            product.images = []
                            product.images.push(response.location)

                            UPDATE_PRODUCT_BY_ID(updatedProduct.id, product)
                            .then(
                                response => {
                                    console.log("UPDated REsponse ", response)
                                    toast.success("Updated Product Successfully !")
                                    setIsLoading(false)
                                }
                            ).catch(error => {
            
                                console.log("Error UPdated Product ", error)
                                toast.error("Failed to update product !")
                                setIsLoading(false)
                            })
                        }
                    )

            } else {


                product.images = updatedProduct.images
                UPDATE_PRODUCT_BY_ID(updatedProduct.id, product).then(
                    response => {
                        console.log("UPDated REsponse ", response)
                        toast.success("Updated Product Successfully !")
                        setIsLoading(false)
                    }
                ).catch(error => {

                    console.log("Error UPdated Product ", error)
                    toast.error("Failed to update product !")
                    setIsLoading(false)
                })


            }




        } else {
            // if the user choose a file or not ? 
            if (selectedFile) {
                // upload file to the server 

                let file = new FormData()
                file.append("file", selectedFile)

                UPLOAD_FILE(file).then(
                    response => {
                        setImages([])
                        setImages(images.push(response.location))
                        product.images = images


                        CREATE_PRODUCT(product).then(
                            response => {
                                console.log("New Product Response : ", response)
                                toast.success("Create Product Successfully!")
                                setIsLoading(false)
                            }
                        ).catch(error => {
                            console.log("Failed to create product : ", error)
                            setIsLoading(false)
                            toast.error("Failed to create product!")
                        })


                    }

                ).catch(error => {
                    console.log("Error uploading files : ", error)
                    toast.error("Failed to upload files !")
                })


                // create product 
            } else {
                CREATE_PRODUCT(product).then(
                    response => {
                        // console.log("New Product Response : ", response)
                        toast.success("Create Product Successfully!")
                        setIsLoading(false)
                    }
                ).catch(error => {
                    console.log("Failed to create product : ", error)
                    setIsLoading(false)
                    toast.error("Failed to create product!")
                })
            }

        }



    }


    const handleImageChange = (e) => {

        setSelectedFile(e.target.files[0])
        let imageUrl = URL.createObjectURL(e.target.files[0])
        setSelectedImage(imageUrl)

    }
    return (
        <Modal size="xl" show={showProduct} onHide={handleProductClose}


        >
            <Modal.Header closeButton={false}>
                <Modal.Title className="m-auto">
                    {updatedProduct ?
                        "Update Product Information " :
                        "Create New Product "
                    }


                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="d-flex">
                    <div className="img-side w-50">
                        <label htmlFor="file-input">
                            <img
                                id="preview-image"
                                className="img-fluid img-thumbnail"
                                width="400px "
                                src={

                                    selectedImage ?
                                        selectedImage :
                                        "https://cdn3d.iconscout.com/3d/premium/thumb/image-6073772-4996992.png"
                                }
                                alt="Profile Picture"
                            />


                        </label>

                        {/* here we listing a smaller images  */}
                        <div className="more-images">
                            <div className="d-flex justify-content-center align-items-center">




                                <div className="d-flex justify-content-center align-items-center position-relative">
                                    <img

                                        className="img-fluid img-thumbnail"
                                        width="50px "
                                        src={"https://cdn3d.iconscout.com/3d/premium/thumb/image-6073772-4996992.png"}
                                        alt="Profile Picture"
                                    />
                                    <div

                                        className=" position-absolute"
                                        style={{ top: '-15px', right: '-5px' }}>


                                        <button className="btn btn-danger btn-sm   rounded-circle"  >
                                            <FontAwesomeIcon icon={faBan} />
                                        </button>
                                    </div>
                                </div>




                            </div>
                        </div>
                        {/* d-none */}
                        <input
                            className="form-control d-none "
                            type="file"
                            name=""
                            onChange={handleImageChange}
                            id="file-input"
                            multiple
                        />
                    </div>

                    <div className="d-flex w-100   ms-3 flex-column">

                        <div className="d-flex mb-3   align-items-center justify-content-between">
                            <div class="form-floating  ">
                                <input
                                    type="text"
                                    class="form-control"
                                    id="floatingInput"
                                    placeholder="Jonh Doe"
                                    value={title}
                                    onChange={e => setTitle(e.target.value)}


                                />
                                <label for="floatingInput" className="input-label">
                                    <div className="d-flex align-items-center">
                                        <FaBox />
                                        <span className="ms-2"> Produce Title  </span>
                                    </div>
                                </label>
                            </div>
                            <div class="form-floating  ">
                                <input
                                    type="number"
                                    class="form-control"
                                    id="floatingInput"
                                    placeholder="Jonh Doe"
                                    value={price}
                                    onChange={e => setPrice(e.target.value)}


                                />
                                <label for="floatingInput" className="input-label">
                                    <div className="d-flex align-items-center">
                                        <FaMoneyBill />
                                        <span className="ms-2"> Produce Price  </span>
                                    </div>
                                </label>
                            </div>


                        </div>


                        <div class="form-floating mb-3  ">
                            <textarea
                                cols={3}
                                type="text"
                                class="form-control"
                                id="floatingInput"
                                value={description}
                                onChange={e => setDescription(e.target.value)}
                                placeholder="name@example.com"
                                style={{ height: "170px" }}

                            ></textarea>
                            <label for="floatingInput" className="input-label">
                                <div className="d-flex align-items-center">
                                    <FaInfo />
                                    <span className="ms-2"> Product Description </span>
                                </div>
                            </label>
                        </div>





                        <Button
                            variant={updatedProduct ? "warning" : "success"}
                            className="w-100 mt-2 mb-5   "
                            onClick={handleCreateNewProduct}

                        >

                            {

                                isLoading ?
                                    <>
                                        <div className="d-flex justify-content-center">
                                            <ThreeDots
                                                height="32"
                                                width="32"
                                                radius="9"
                                                color="#fff"
                                                ariaLabel="three-dots-loading"
                                                wrapperStyle={{}}
                                                wrapperClassName=""
                                                visible={true}
                                            />
                                        </div>
                                    </>
                                    :
                                    <>
                                        {
                                            updatedProduct ?
                                                "Update Product"
                                                :
                                                "Create Product"
                                        }
                                    </>
                            }



                        </Button>
                    </div>
                </div>
            </Modal.Body>
        </Modal>
    )
}

export default ProductModal