#!/bin/bash

# List of Node.js versions
NODE_VERSIONS=("18-alpine" "16-alpine" "14-alpine")

# Try to build the Docker image for each Node.js version
for VERSION in "${NODE_VERSIONS[@]}"
do
 echo "Building Docker image for Node.js version $VERSION"
 docker build --build-arg NODE_VERSION=$VERSION -t myapp:$VERSION . && { echo "Successfully built Docker image for Node.js version $VERSION"; break; }
done

