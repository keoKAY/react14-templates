FROM node:lts-alpine3.19 
COPY .  . 
RUN npm install 
RUN npm run build 
EXPOSE 3000 
CMD ["npm","start"]

 # docker build -t reactjs-custom-image  .
 # docker run -d -p 3001:3000 --name reactjs-container-new reactjs-custom-image

# make the Spring code become public 
# https://gitlab.com/personal-project5523607/microservices-spring/simple-product-service